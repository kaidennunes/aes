1. Run and Compile:
	- Open the solution file (.sln) in Visual Studio
	- Click the green arrow (or click F5)

2. I used only the resources listed in the project 1 section

3. All the test cases in Appendix C pass (the test cases and the results of each step of the test cases is printed out automatically upon running the program. The whitespace may be slightly different)