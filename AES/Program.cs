﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AES
{
	class Program
	{
		static void Main(string[] args)
		{
			// Example from flash animation (this example uses row ordering, not column ordering like the FIPS examples)
			//RunTest(new byte[16] { 0x32, 0x88, 0x31, 0xe0, 0x43, 0x5a, 0x31, 0x37, 0xf6, 0x30, 0x98, 0x07, 0xa8, 0x8d, 0xa2, 0x34 },
			//	new byte[16] { 0x2b, 0x28, 0xab, 0x09, 0x7e, 0xae, 0xf7, 0xcf, 0x15, 0xd2, 0x15, 0x4f, 0x16, 0xa6, 0x88, 0x3c });


			// Examples from FIPS 
			RunTest(new byte[16] { 0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xaa, 0xbb, 0xcc, 0xdd, 0xee, 0xff },
				new byte[16] { 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f });

			RunTest(new byte[16] { 0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xaa, 0xbb, 0xcc, 0xdd, 0xee, 0xff },
				new byte[24] { 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f, 0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17 });

			RunTest(new byte[16] { 0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xaa, 0xbb, 0xcc, 0xdd, 0xee, 0xff },
				new byte[32] { 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f, 0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x1a, 0x1b, 0x1c, 0x1d, 0x1e, 0x1f });

			Console.Read();
		}

		/// <summary>
		/// Runs a test with encrypting and decrypting plain text using a given key
		/// </summary>
		/// <param name="plainBytes">The bytes to be encrypted.</param>
		/// <param name="key">The key to be used to encrypt/decrypt the text.</param>
		static void RunTest(byte[] plainBytes, byte[] key)
		{
			Console.WriteLine("Running Test");

			AES cryptographer = new AES(key);

			Console.WriteLine("Encrypting");
			byte[] encryptedBytes = cryptographer.EncryptText(plainBytes);

			Console.WriteLine();
			Console.WriteLine("Decrypting");
			byte[] decryptedBytes = cryptographer.DecryptText(encryptedBytes);

			Console.WriteLine();
		}
	}
}
