﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AES
{
	public class AES
	{
		private Key Key { get; set; }

		private static readonly Encoding ENCODING = Encoding.ASCII;

		public AES(string key) : this(ENCODING.GetBytes(key)) { }

		public AES(byte[] key)
		{
			SetKey(key);
		}

		public void SetKey(string key)
		{
			SetKey(Encoding.ASCII.GetBytes(key));
		}

		public void SetKey(byte[] key)
		{
			Key = new Key(key);
		}

		public string EncryptText(string plainText)
		{
			return ENCODING.GetString(EncryptText(Encoding.ASCII.GetBytes(plainText)));
		}

		public byte[] EncryptText(byte[] plainBytes)
		{
			// Turn the bytes into a 2d array
			byte[,] stateByteArray = Utils.Make2DArrayByVectors(plainBytes, 4, 4);

			// Create the initial state
			State state = new State
			{
				CurrentState = stateByteArray
			};

			// Perform the cipher on the state, using the key
			PerformCipher(state);

			// Convert the bytes back into a 1d array
			return Utils.Make1DArray(state.CurrentState);
		}

		public string DecryptText(string encryptedText)
		{
			return ENCODING.GetString(DecryptText(Encoding.ASCII.GetBytes(encryptedText)));
		}

		public byte[] DecryptText(byte[] encryptedBytes)
		{
			// Turn the bytes into a 2d array (we don't use the vector based one 
			// because when we converted the matrix back to a 1d array, we converted it in a smart way, so it would be by rows and not by columns)
			byte[,] stateByteArray = Utils.Make2DArray(encryptedBytes, 4, 4);

			// Create the initial state
			State state = new State
			{
				CurrentState = stateByteArray
			};

			// Perform the cipher on the state, using the key
			PerformInvCipher(state);

			// Convert the bytes back into a 1d array
			return Utils.Make1DArray(state.CurrentState);
		}

		private void PerformCipher(State state)
		{
			Utils.PrintBytesByColumn(state.CurrentState, 4, 4, "round[ 0].input");

			byte[,] keyUsed = state.AddRoundKey(Key.KeyBytes);
			Utils.PrintBytes(keyUsed, 4, 4, "round[ 0].k_sch");

			for (int i = 1; i < Key.NumberOfRounds; i++)
			{
				Utils.PrintBytesByColumn(state.CurrentState, 4, 4, "round[ " + i + "].start");

				state.SubBytes();
				Utils.PrintBytesByColumn(state.CurrentState, 4, 4, "round[ " + i + "].s_box");

				state.ShiftRows();
				Utils.PrintBytesByColumn(state.CurrentState, 4, 4, "round[ " + i + "].s_row");

				state.MixColumns();
				Utils.PrintBytesByColumn(state.CurrentState, 4, 4, "round[ " + i + "].m_col");

				keyUsed = state.AddRoundKey(Key.KeyBytes, i);
				Utils.PrintBytes(keyUsed, 4, 4, "round[ " + i + "].k_sch");

			}

			Utils.PrintBytesByColumn(state.CurrentState, 4, 4, "round[ " + Key.NumberOfRounds + "].start");

			state.SubBytes();
			Utils.PrintBytesByColumn(state.CurrentState, 4, 4, "round[ " + Key.NumberOfRounds + "].s_box");

			state.ShiftRows();
			Utils.PrintBytesByColumn(state.CurrentState, 4, 4, "round[ " + Key.NumberOfRounds + "].s_row");

			keyUsed = state.AddRoundKey(Key.KeyBytes, Key.NumberOfRounds);
			Utils.PrintBytes(keyUsed, 4, 4, "round[ " + Key.NumberOfRounds  + "].k_sch");

			Utils.PrintBytesByColumn(state.CurrentState, 4, 4, "round[ " + Key.NumberOfRounds + "].output");
		}

		private void PerformInvCipher(State state)
		{
			Utils.PrintBytesByColumn(state.CurrentState, 4, 4, "round[ 0].iinput");

			byte[,] keyUsed = state.AddRoundKey(Key.KeyBytes, Key.NumberOfRounds);
			Utils.PrintBytes(keyUsed, 4, 4, "round[ 0].ik_sch");

			for (int i = Key.NumberOfRounds - 1; i > 0; i--)
			{
				Utils.PrintBytesByColumn(state.CurrentState, 4, 4, "round[ " + (Key.NumberOfRounds - i) + "].istart");

				state.InvShiftRows();
				Utils.PrintBytesByColumn(state.CurrentState, 4, 4, "round[ " + (Key.NumberOfRounds - i) + "].is_row");

				state.InvSubBytes();
				Utils.PrintBytesByColumn(state.CurrentState, 4, 4, "round[ " + (Key.NumberOfRounds - i) + "].is_box");

				keyUsed = state.AddRoundKey(Key.KeyBytes, i);
				Utils.PrintBytes(keyUsed, 4, 4, "round[ " + (Key.NumberOfRounds - i) + "].ik_sch");

				Utils.PrintBytesByColumn(state.CurrentState, 4, 4, "round[ " + (Key.NumberOfRounds - i) + "].ik_add");

				state.InvMixColumns();
			}

			Utils.PrintBytesByColumn(state.CurrentState, 4, 4, "round[ " + Key.NumberOfRounds + "].istart");

			state.InvShiftRows();
			Utils.PrintBytesByColumn(state.CurrentState, 4, 4, "round[ " + Key.NumberOfRounds + "].is_row");

			state.InvSubBytes();
			Utils.PrintBytesByColumn(state.CurrentState, 4, 4, "round[ " + Key.NumberOfRounds + "].is_box");

			keyUsed = state.AddRoundKey(Key.KeyBytes);
			Utils.PrintBytes(keyUsed, 4, 4, "round[ " + Key.NumberOfRounds + "].ik_sch");

			Utils.PrintBytesByColumn(state.CurrentState, 4, 4, "round[ " + Key.NumberOfRounds + "].ioutput");

		}
	}
}
