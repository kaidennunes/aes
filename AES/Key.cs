﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AES
{
	public class Key
	{
		public byte[,] KeyBytes { get; set; }

		private int NumberOfWordsInKey { get; set; }

		public int NumberOfRounds { get; private set; }

		private static readonly int EXTRA_ROUNDS_CONSTANT = 6;

		public Key(byte[] key)
		{
			var numberOfWordsInKey = (key.Length / 4);
			AssertCorrectKeyLength(numberOfWordsInKey);

			NumberOfWordsInKey = numberOfWordsInKey;
			NumberOfRounds = NumberOfWordsInKey + EXTRA_ROUNDS_CONSTANT;
			KeyBytes = ExpandKey(Utils.Make2DArrayByVectors(key, 4, numberOfWordsInKey), NumberOfWordsInKey, NumberOfRounds);
		}

		private void AssertCorrectKeyLength(int numberOfWordsInKey)
		{
			if (numberOfWordsInKey == 4)
			{
			}
			else if (numberOfWordsInKey == 6)
			{

			}
			else if (numberOfWordsInKey == 8)
			{

			}
			else
			{
				throw new ArgumentException("Argument is not a valid for the number of 32-bit words in key");
			}
		}

		private byte[,] ExpandKey(byte[,] key, int keyLength, int numberOfRounds)
		{
			byte[,] newKey = new byte[4, (4 * (numberOfRounds + 1))];
			byte[] temp = new byte[4];

			for (int i = 0; i < keyLength; i++)
			{
				newKey[0, i] = key[0, i];
				newKey[1, i] = key[1, i];
				newKey[2, i] = key[2, i];
				newKey[3, i] = key[3, i];
			}

			for (int i = keyLength; i < 4 * (numberOfRounds + 1); i++)
			{
				newKey[0, i] = 0;
				newKey[1, i] = 0;
				newKey[2, i] = 0;
				newKey[3, i] = 0;
				for (var j = 0; j < 4; j++)
				{
					temp[j] = newKey[j, i - 1];
				}

				if (i % keyLength == 0)
				{
					temp = SubWord(RotWord(temp));
					for (var j = 0; j < 4; j++)
					{
						temp[j] ^= MatrixConstants.ROUND_BOX[(i / keyLength), j];
					}
				}
				else if (keyLength > 6 && i % keyLength == 4)
				{
					temp = SubWord(temp);
				}

				for (var j = 0; j < 4; j++)
				{
					newKey[j, i] = (byte)(newKey[j, (i - keyLength)] ^ temp[j]);
				}
			}
			return newKey;
		}

		private byte[] SubWord(byte[] values)
		{
			return new byte[4] { MatrixConstants.S_BOX[values[0]], MatrixConstants.S_BOX[values[1]], MatrixConstants.S_BOX[values[2]], MatrixConstants.S_BOX[values[3]] };
		}

		private byte[] RotWord(byte[] values)
		{
			return new byte[4] { values[1], values[2], values[3], values[0] };
		}
	}
}
