﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AES
{
	public static class Utils
	{
		public static T[,] Make2DArray<T>(T[] input, int height, int width)
		{
			T[,] output = new T[height, width];
			for (int i = 0; i < height; i++)
			{
				for (int j = 0; j < width; j++)
				{
					output[i, j] = input[i * width + j];
				}
			}
			return output;
		}

		public static T[,] Make2DArrayByVectors<T>(T[] input, int height, int width)
		{
			T[,] output = new T[height, width];
			for (int i = 0; i < width; i++)
			{
				for (int j = 0; j < height; j++)
				{
					output[j, i] = input[i * height + j];
				}
			}
			return output;
		}

		public static T[] Make1DArray<T>(T[,] input)
		{
			T[] output = new T[input.Length];
			Buffer.BlockCopy(input, 0, output, 0, input.Length);
			return output;
		}

		public static void PrintBytes(byte[,] bytes, int height, int width, string header = "")
		{
			Console.Write(header + " ");
			for (int i = 0; i < height; i++)
			{
				for (int j = 0; j < width; j++)
				{
					Console.Write(bytes[i, j].ToString("x2"));
				}
			}
			Console.WriteLine();
		}

		public static void PrintBytesByColumn(byte[,] bytes, int height, int width, string header = "")
		{
			Console.Write(header + " ");
			for (int i = 0; i < width; i++)
			{
				for (int j = 0; j < height; j++)
				{
					Console.Write(bytes[j, i].ToString("x2"));
				}
			}
			Console.WriteLine();
		}
	}
}
