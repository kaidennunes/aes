﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AES
{
	public class State
	{
		public byte[,] CurrentState { get; set; }

		public byte[,] AddRoundKey(byte[,] key, int round = 0)
		{
			byte[,] keyUsed = new byte[4, 4];
			for (int i = 0; i < 4; i++)
			{
				for (int j = 0; j < 4; j++)
				{
					keyUsed[i, j] = key[j, i + round * 4];
					CurrentState[j, i] = (byte)(CurrentState[j, i] ^ key[j, i + round * 4]);
				}
			}
			return keyUsed;
		}

		public void SubBytes()
		{
			for (int i = 0; i < 4; i++)
			{
				for (int j = 0; j < 4; j++)
				{
					CurrentState[i, j] = MatrixConstants.S_BOX[CurrentState[i, j]];
				}
			}
		}

		public void ShiftRows()
		{
			for (int i = 1; i < 4; i++)
			{
				byte[] row = new byte[4] {
					CurrentState[i, 0],
					CurrentState[i, 1],
					CurrentState[i, 2],
					CurrentState[i, 3]
				};

				CurrentState[i, 0] = row[(0 + i) % 4];
				CurrentState[i, 1] = row[(1 + i) % 4];
				CurrentState[i, 2] = row[(2 + i) % 4];
				CurrentState[i, 3] = row[(3 + i) % 4];
			}
		}

		public void MixColumns()
		{
			// Create a temporary array to store the calculations
			byte[,] tempState = new byte[4, 4];

			// For each column in the state, perform the multiplications using the matrix provided by AES standards
			for (int i = 0; i < 4; i++)
			{
				tempState[0, i] = (byte)(FiniteFieldMultiply(0x02, CurrentState[0, i]) ^ FiniteFieldMultiply(0x03, CurrentState[1, i]) ^ CurrentState[2, i] ^ CurrentState[3, i]);
				tempState[1, i] = (byte)(CurrentState[0, i] ^ FiniteFieldMultiply(0x02, CurrentState[1, i]) ^ FiniteFieldMultiply(0x03, CurrentState[2, i]) ^ CurrentState[3, i]);
				tempState[2, i] = (byte)(CurrentState[0, i] ^ CurrentState[1, i] ^ FiniteFieldMultiply(0x02, CurrentState[2, i]) ^ FiniteFieldMultiply(0x03, CurrentState[3, i]));
				tempState[3, i] = (byte)(FiniteFieldMultiply(0x03, CurrentState[0, i]) ^ CurrentState[1, i] ^ CurrentState[2, i] ^ FiniteFieldMultiply(0x02, CurrentState[3, i]));
			}

			// Copy the content of the temporary array (our results) to the actual state
			CurrentState = tempState;
		}

		public void InvSubBytes()
		{
			for (int i = 0; i < 4; i++)
			{
				for (int j = 0; j < 4; j++)
				{
					CurrentState[i, j] = MatrixConstants.INV_S_BOX[CurrentState[i, j]];
				}
			}
		}

		public void InvShiftRows()
		{
			for (int i = 1; i < 4; i++)
			{
				byte[] row = new byte[4] {
					CurrentState[i, 0],
					CurrentState[i, 1],
					CurrentState[i, 2],
					CurrentState[i, 3]
				};

				CurrentState[i, 0] = row[(4 - i) % 4];
				CurrentState[i, 1] = row[(5 - i) % 4];
				CurrentState[i, 2] = row[(6 - i) % 4];
				CurrentState[i, 3] = row[(7 - i) % 4];
			}
		}

		public void InvMixColumns()
		{
			// Create a temporary array to store the calculations
			byte[,] tempState = new byte[4, 4];

			// For each column in the state, perform the multiplications using the matrix provided by AES standards
			for (int i = 0; i < 4; i++)
			{
				tempState[0, i] = (byte)(FiniteFieldMultiply(0x0e, CurrentState[0, i]) ^ FiniteFieldMultiply(0x0b, CurrentState[1, i]) ^ FiniteFieldMultiply(0x0d, CurrentState[2, i]) ^ FiniteFieldMultiply(0x09, CurrentState[3, i]));
				tempState[1, i] = (byte)(FiniteFieldMultiply(0x09, CurrentState[0, i]) ^ FiniteFieldMultiply(0x0e, CurrentState[1, i]) ^ FiniteFieldMultiply(0x0b, CurrentState[2, i]) ^ FiniteFieldMultiply(0x0d, CurrentState[3, i]));
				tempState[2, i] = (byte)(FiniteFieldMultiply(0x0d, CurrentState[0, i]) ^ FiniteFieldMultiply(0x09, CurrentState[1, i]) ^ FiniteFieldMultiply(0x0e, CurrentState[2, i]) ^ FiniteFieldMultiply(0x0b, CurrentState[3, i]));
				tempState[3, i] = (byte)(FiniteFieldMultiply(0x0b, CurrentState[0, i]) ^ FiniteFieldMultiply(0x0d, CurrentState[1, i]) ^ FiniteFieldMultiply(0x09, CurrentState[2, i]) ^ FiniteFieldMultiply(0x0e, CurrentState[3, i]));
			}

			// Copy the content of the temporary array (our results) to the actual state
			CurrentState = tempState;
		}

		private byte FiniteFieldAdd(byte firstByte, byte secondByte)
		{
			return (byte)(firstByte ^ secondByte);
		}

		private byte FiniteFieldMultiply(byte firstByte, byte secondByte)
		{
			byte product = 0;

			for (int counter = 0; counter < 8; counter++)
			{
				// If the smallest bit (of the first byte) is set, then do XOR with the first byte (i.e addition)
				if ((secondByte & 1) != 0)
				{
					product ^= firstByte;
				}

				// Check to see if the highest bit is set
				bool highestBitSet = (firstByte & 0x80) != 0;

				// Left bit shift the first byte by one
				firstByte <<= 1;

				// If the highest bit was set (i.e we had carry over from the left bit shift), do XOR with the irreducible polyonmial (x^8 + x^4 + x^3 + x + 1)
				if (highestBitSet)
				{
					// 0x1B represents the irreducible polynomial
					firstByte ^= 0x1B;
				}

				// Right bit shift the second byte by one
				secondByte >>= 1;
			}
			return product;
		}
	}
}
